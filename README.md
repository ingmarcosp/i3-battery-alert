i3-Battery-Alert
====================

This script controls the battery's percentage, by default will check every 10 minutes and
alert you when the battery reached the 10%, and the check will be in the half time.

You can setup this two values, but when the battery reach the 3% will check
every minute the battery.

The first parameter is battery "PERCENT" and the second is "MINUTES".

`~> python3 battery-alert.py 30 20`

Requirements:
- psutil
- ogg123

Instalation
=============

Copy `battery-alert.py` to your `home/user/bin` directory:

`~> cp battery-alert.py ~/bin`
`~> cp *.ogg ~/bin`

Then add this line to your i3wm config file, usualy in `~/.config/i3/config`

`exec --no-startup-id python3 /home/user/bin/battery-alert.py`
