"""i3-Battery-Alert

This script controls the battery's percentage, by default will check every 10 minutes and
alert you when the battery reached the 10%, and the check will be in the half time. 
You can setup this two values, but when the battery reach the 3% will check 
every minute the battery.

Requirements:
i3-nagbar
ogg123
"""

import sys, os, threading
from time import sleep
from psutil import sensors_battery
from datetime import timedelta

BAT = 10 # % PERCENT
TIME = 10 # MINUTES

if len(sys.argv) > 1:
    try:
        BAT = int(sys.argv[1])
        TIME = int(sys.argv[2])
    except ValueError:
        print("Only integers can be used to set battery porcentage and time.\n Using defoult values.")
    except IndexError:
        pass

WARNING = "i3-nagbar -t warning -m 'Your battery is under {0}, you have {1} left. Do you want to exit? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"
ALERT = "i3-nagbar -t alert -m 'Your battery is under {0}. Your PC will to POWEROFF SOON!'"

def battery_checker():
    while True:
        pluggedin = sensors_battery().power_plugged
        percent = int(sensors_battery().percent)
        if percent < 3 and not pluggedin:
            threading.Thread(target=os.system, args=("ogg123 alert.ogg", )).start()
            threading.Thread(target=os.system, args=(ALERT.format(BAT), )).start()
            sleep(90)
            pass
        elif percent <= BAT and not pluggedin:
            time_left = str(timedelta(seconds=sensors_battery().secsleft))
            threading.Thread(target=os.system, args=("ogg123 warning.ogg", )).start()
            threading.Thread(target=os.system, args=(WARNING.format(BAT, time_left), )).start()
            sleep((TIME/2)*60)
            pass
        sleep(TIME*60)

if __name__ == "__main__":
    battery_checker()
